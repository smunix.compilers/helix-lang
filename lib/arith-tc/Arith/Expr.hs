module Arith.Expr where

import Arith.Op
import Data.Sequence (Seq)

data Prim where
  Bool :: Bool -> Prim
  Int :: Int -> Prim
  Float :: Float -> Prim
  Double :: Double -> Prim
  deriving (Show)

data Expr where
  Prim :: Prim -> Expr
  Bin :: Bin -> Seq Expr -> Expr
  Una :: Una -> Expr -> Expr
  If :: Expr -> Expr -> Expr -> Expr
  deriving (Show)
