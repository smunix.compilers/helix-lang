{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}

module Arith.Internal.TypeCheck.Playground where

import Control.Monad.Error.Class (MonadError (throwError))
import Data.Maybe (fromMaybe)
import Data.Sequence (Seq)
import Optics

data Op where
  Add :: Op
  Mul :: Op
  deriving (Show)

data E where
  MkInt :: Int -> E
  MkBool :: Bool -> E
  MkFold :: Op -> Seq E -> E
  deriving (Show)

data Prim a where
  Int :: Prim Int
  Bool :: Prim Bool

deriving instance Show (Prim a)

data Ty a where
  Prim :: Prim a -> Ty a

deriving instance Show (Ty a)

data E' a where
  Mk'Int :: Int -> E' Int
  Mk'Bool :: Bool -> E' Bool
  Mk'Fold :: Op -> Seq (E' a) -> E' a

deriving instance Show (E' a)

data TcE where
  TcE :: Ty a -> E' a -> TcE

deriving instance Show TcE

newtype InferM a where
  InferM :: {unInferM :: Either String a} -> InferM a
  deriving (Functor, Applicative, Monad, MonadError String)

data Rfl a b where
  Rfl :: Rfl a a

class IsSame c a b where
  isSame :: c a -> c b -> Maybe (Rfl a b)

instance IsSame Ty a b where
  isSame (Prim a) (Prim b) = isSame a b

instance IsSame Prim a b where
  isSame Int Int = pure Rfl
  isSame Bool Bool = pure Rfl
  isSame _ _ = Nothing

infer :: E -> InferM TcE
infer (MkInt i) = pure $ TcE (Prim Int) (Mk'Int i)
infer (MkBool b) = pure $ TcE (Prim Bool) (Mk'Bool b)
infer (MkFold op ues) = do
  ues
    & traverseOf traversed infer
    >>= \case
      Empty -> errEmpty
      tces -> foldrOf folded infer' Nothing tces & fromMaybe errUnknown
  where
    errEmpty :: InferM TcE
    errEmpty = throwError "Empty MkFold"

    errUnknown :: InferM TcE
    errUnknown = throwError "Unknown error"

    errUnexpected :: InferM TcE
    errUnexpected = throwError "Unexpected type"

    errMismatch :: InferM TcE
    errMismatch = throwError "Type mismatch"

    infer' :: TcE -> Maybe (InferM TcE) -> Maybe (InferM TcE)
    infer' (TcE ety e') Nothing = pure . pure $ TcE ety (Mk'Fold op $ e' :< Empty)
    infer' (TcE ety e') (Just asM) = pure do
      TcE aty as <- asM
      isSame ety aty & maybe
        errMismatch
        \Rfl -> case as of
          Mk'Fold _ as' -> pure $ TcE aty (Mk'Fold op $ e' :< as')
          _ -> errUnexpected
