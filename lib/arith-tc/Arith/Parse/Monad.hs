{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Arith.Parse.Monad where

import Arith.Alex (AlexInput (AlexInput), AlexSourcePos (AlexSourcePos))
import Control.Monad.Error.Class (MonadError)
import Control.Monad.State.Class (MonadState)
import Control.Monad.Trans.State (StateT, evalStateT)
import Optics

newtype M e a where
  M :: {_run :: StateT AlexInput (Either e) a} -> M e a
  deriving (Functor, Applicative, Monad, MonadState AlexInput, MonadError e)

makeLenses ''M

parse :: M e a -> String -> Either e a
parse p str = evalStateT (p ^. run) (AlexInput (AlexSourcePos 1 1) '\n' Empty str)
