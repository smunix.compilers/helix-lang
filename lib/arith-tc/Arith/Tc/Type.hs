{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}

module Arith.Tc.Type where

data P a where
  Bool :: P Bool
  Int :: P Int
  Float :: P Float
  Double :: P Double

deriving instance Show (P a)

data Ty a where
  Prim :: P a -> Ty a

deriving instance Show (Ty a)

data Refl a b where
  Refl :: Refl a a

class IsSame c a b where
  isSame :: c a -> c b -> Maybe (Refl a b)

instance IsSame P a b where
  isSame Bool Bool = pure Refl
  isSame Int Int = pure Refl
  isSame Float Float = pure Refl
  isSame Double Double = pure Refl
  isSame _ _ = Nothing

instance IsSame Ty a b where
  isSame (Prim a) (Prim b) = isSame a b
