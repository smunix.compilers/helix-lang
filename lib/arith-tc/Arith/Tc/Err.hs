{-# LANGUAGE StandaloneDeriving #-}

module Arith.Tc.Err where

import Data.Sequence (Seq)

data Fail where
  Fail :: (Show v) => v -> Fail

deriving instance Show Fail

data Err where
  Unknown :: (Show e) => e -> Err
  Unexpected :: (Show a) => a -> Err
  Mismatch :: (Show a, Show b) => a -> b -> Err
  Failed :: Seq Fail -> Err

deriving instance Show Err
