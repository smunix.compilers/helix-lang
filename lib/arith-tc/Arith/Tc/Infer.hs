module Arith.Tc.Infer where

import qualified Arith.Expr as U
import Arith.Tc.Err (Err (..), Fail (Fail))
import Arith.Tc.Expr (Expr (Expr))
import qualified Arith.Tc.Expr as E
import qualified Arith.Tc.Type as Ty
import Control.Monad.Error.Class (MonadError (throwError))
import Data.Maybe (fromMaybe)
import qualified Data.Sequence as Seq
import Optics

newtype M a where
  M :: {_run :: Either Err a} -> M a
  deriving (Functor, Applicative, Monad, MonadError Err)

makeLenses ''M

class Infer a where
  infer :: a -> M Expr

instance Infer U.Expr where
  infer (U.Prim up) = infer up
  infer v@(U.Bin op ues) =
    ues & traverseOf traversed infer >>= \case
      Empty -> throwError . Unknown $ v
      tces -> foldrOf folded infer' Nothing tces & fromMaybe (throwError . Unknown $ "Unknown error")
    where
      infer' :: Expr -> Maybe (M Expr) -> Maybe (M Expr)
      infer' (Expr ty e) Nothing = pure . pure $ Expr ty (E.Bin op $ e :< Empty)
      infer' (Expr ty e) (Just esM) = pure do
        Expr ety es <- esM
        Ty.isSame ety ty & maybe
          (throwError $ Mismatch ty ety)
          \Ty.Refl -> case es of
            E.Bin _ es' -> pure $ Expr ety (E.Bin op $ e :< es')
            v' -> throwError . Failed . Seq.fromList $ [Fail v', Fail (ty, e)]
  infer (U.Una op ue) = do
    Expr ty e <- infer ue
    pure $ Expr ty (E.Una op e)
  infer (U.If ucond ut uf) = do
    Expr ty cond <- infer ucond
    Ty.isSame ty bool & maybe
      (throwError $ Mismatch ty bool)
      \Ty.Refl -> do
        Expr tty t <- infer ut
        Expr fty f <- infer uf
        Ty.isSame tty fty & maybe
          (throwError $ Mismatch tty fty)
          \Ty.Refl -> pure $ Expr tty (E.If cond t f)
    where
      bool = Ty.Prim Ty.Bool

-- | Construct a type checked from untyped primitive expressions
mkPrim :: Ty.P a -> (v -> E.P a) -> v -> M Expr
mkPrim ty ctor v = pure $ Expr (Ty.Prim ty) (E.Prim (ctor v))

instance Infer U.Prim where
  infer (U.Bool b) = mkPrim Ty.Bool E.Bool b
  infer (U.Int i) = mkPrim Ty.Int E.Int i
  infer (U.Float f) = mkPrim Ty.Float E.Float f
  infer (U.Double d) = mkPrim Ty.Double E.Double d
