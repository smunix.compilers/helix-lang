{
module Arith.Lexer where

import Prelude hiding (lex)

import Data.Bits ((.&.), shiftR)
import Data.Char (ord)
import Data.Word (Word8)

import Optics
import Optics.State.Operators

import qualified Control.Monad.Except as E
import Arith.Error
import Arith.Alex
import Arith.Parse.Monad
}

$digit = 0-9
$lower = a-z
$upper = A-Z
$alpha = [$lower $upper]
$alphanum = [$alpha $digit]
@ident = $lower [$lower $upper _ ']*

tokens :-

-- Whitespace insensitive
$white+ ;

-- Comments
"--".* \n ;
\n ;

-- Syntax
<0> {
  \-? $digit+ d            { \sp s@(preview _Snoc -> Just (s',_)) -> pure $ Loc sp (Double $ read s') }
  \-? $digit+ \. $digit+ d { \sp s@(preview _Snoc -> Just (s',_)) -> pure $ Loc sp (Double $ read s') }
  \-? $digit+ \. d         { \sp s@(preview _Snoc -> Just (s',_)) -> pure $ Loc sp (Double $ read (s' :> '0')) }
  \-? $digit+ f            { \sp s@(preview _Snoc -> Just (s',_)) -> pure $ Loc sp (Float $ read s') }
  \-? $digit+ \. $digit+ f { \sp s@(preview _Snoc -> Just (s',_)) -> pure $ Loc sp (Float $ read s') }
  \-? $digit+ \. f         { \sp s@(preview _Snoc -> Just (s',_)) -> pure $ Loc sp (Float $ read (s' :> '0')) }
  \-? $digit+              { \sp s -> pure $ Loc sp (Int $ read s) }
  \-? $digit+ \. $digit+   { \sp s -> pure $ Loc sp (Float $ read s) }
  \-? $digit+ \.           { \sp s -> pure $ Loc sp (Float $ read (s :> '0')) }
  \+                       { \sp _ -> pure $ Loc sp Add }
  \-                       { \sp _ -> pure $ Loc sp Neg }
  \*                       { \sp _ -> pure $ Loc sp Mul }
  \/                       { \sp _ -> pure $ Loc sp Slash }
  \(                       { \sp _ -> pure $ Loc sp OpenParen }
  \)                       { \sp _ -> pure $ Loc sp CloseParen }
  \[                       { \sp _ -> pure $ Loc sp OpenBracket }
  \]                       { \sp _ -> pure $ Loc sp CloseBracket }
  \,                       { \sp _ -> pure $ Loc sp Comma }
  "if"                     { \sp _ -> pure $ Loc sp If }
  "then"                   { \sp _ -> pure $ Loc sp Then }
  "else"                   { \sp _ -> pure $ Loc sp Else }
  ("true" | T | t | "True") { \sp _ -> pure $ Loc sp (Bool True) }
  ("false" | F | f | "False") { \sp _ -> pure $ Loc sp (Bool False) }
}

{
utf8Encode :: Char -> [Word8]
utf8Encode = go . fromIntegral . ord
  where
    go w
      | w <= 0x7f  = w            :< Empty
      | w < 0x7ff  = (0xc0 + w6)  :< (fn <$> [w])
      | w < 0x7fff = (0xe0 + w12) :< (fn <$> [w6, w])
      | otherwise  = (0xf0 + w18) :< (fn <$> [w12, w6, w])
      where
        w6 = w `shiftR` 6
        w12 = w `shiftR` 12
        w18 = w `shiftR` 18
        w3f = w .&. 0x3f
        fn = (+ 0x80) . (.&. 0x3f)

alexStartPos :: AlexSourcePos
alexStartPos = AlexSourcePos 1 1

alexMove :: AlexSourcePos -> Char -> AlexSourcePos
alexMove sp '\t' = sp & col %~ \c -> c + alex_tab_size - ((c-1) `mod` alex_tab_size)
alexMove sp '\n' = sp & col .~ 1 & line %~ (+1)
alexMove sp _ = sp & col %~ (+1)

alexGetByte :: AlexInput -> Maybe (Word8, AlexInput)
alexGetByte ai@(preview (pending % _Cons) -> Just (b, bs)) = Just (b, ai & pending .~ bs)
alexGetByte AlexInput { _pending = Empty, _input = Empty } = Nothing
alexGetByte ai@(preview (input % _Cons) -> Just (c@(preview (to utf8Encode % _Cons) -> Just (b, bs)), cs)) = Just (b, ai')
  where
    !p' = alexMove (ai ^. pos) c
    !ai' = ai & input .~ cs & char .~ c & pending .~ bs & pos .~ p'

lex :: M Err (Loc Token)
lex = use equality >>= \ai -> case alexScan ai 0 of
  AlexError ai'@(view pos -> p) -> E.throwError $ LexErr ai'
  AlexSkip ai' _ -> do equality' .= ai'; lex
  AlexToken ai'@(view pos -> p) len act -> do equality' .= ai'; ai ^. input % to (act p . take len)
  AlexEOF -> pure $ Loc (ai ^. pos) EOF
}
