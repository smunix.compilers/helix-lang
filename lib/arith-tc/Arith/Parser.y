{
module Arith.Parser where

import qualified Arith.Lexer as L
import Arith.Alex (Loc(..), Token, AlexInput)
import Arith.Error
import Arith.Op
import qualified Arith.Alex as Tok
import Arith.Expr
import Arith.Parse.Monad    

import Optics
import Optics.State.Operators
import Data.Maybe 
import qualified Control.Monad.Except as E

}

%name arith expr
%tokentype { (Loc Token) }
%error { (E.throwError . ParseErr) }
%lexer { (L.lex >>=) } { (Loc _ Tok.EOF) }
%monad { M Err }

%token
'+'    { Loc _ Tok.Add }
'-'    { Loc _ Tok.Neg }
'*'    { Loc _ Tok.Mul }
'/'    { Loc _ Tok.Slash }
'('    { Loc _ Tok.OpenParen }
')'    { Loc _ Tok.CloseParen }
'['    { Loc _ Tok.OpenBracket }
']'    { Loc _ Tok.CloseBracket }
','    { Loc _ Tok.Comma }
int    { Loc _ (Tok.Int $$) }
float  { Loc _ (Tok.Float $$) }
double { Loc _ (Tok.Double $$) }
bool   { Loc _ (Tok.Bool $$) }
true   { Loc _ (Tok.Bool True) }
false  { Loc _ (Tok.Bool False) }
if     { Loc _ Tok.If }
then   { Loc _ Tok.Then }
else   { Loc _ Tok.Else }

%left '+' 
%left '*'
%left '-'
%right '/'

%%

expr :: { Expr }
     : expr '+' term { Bin (:+:) ($1 :< $3 :< Empty) }
     | expr '-' term { Bin (:+:) ($1 :< (Una (:-:) $3) :< Empty) }
     | term          { $1 }

term :: { Expr }
     : term '*' factor { Bin (:*:) ($1 :< $3 :< Empty) }
     | term '/' factor { Bin (:*:) ($1 :< (Una (:/:) $3) :< Empty) }
     | factor          { $1 }

factor :: { Expr }
       : prim         { $1 }
       | folds        { $1 }
       | '-' factor   { unfoldUna (:-:) (:+:) ($2 :< Empty) }
       | '/' factor   { unfoldUna (:/:) (:*:) ($2 :< Empty) }
       | '+' factor   { unfoldBin (:+:) ($2 :< Empty) }
       | '*' factor   { unfoldBin (:*:) ($2 :< Empty) }
       | '(' expr ')' { $2 }
       | cond         { $1 }

prim :: { Expr }
       : int          { Prim (Int $1) }
       | float        { Prim (Float $1) }
       | double       { Prim (Double $1) }
       | bool         { Prim (Bool $1) }

cond :: { Expr }
       : if expr then expr else expr { If $2 $4 $6 }

some(p) : some(p) ',' p { $1 :> $3 }
        | p             { $1 :< Empty }
    
folds :: { Expr }
folds : '+' '[' some(expr) ']' { unfoldBin (:+:) $3 }
      | '-' '[' some(expr) ']' { unfoldUna (:-:) (:+:) $3 }
      | '*' '[' some(expr) ']' { unfoldBin (:*:) $3 }
      | '/' '[' some(expr) ']' { unfoldUna (:/:) (:*:) $3 }

{
unfoldUna uop bop seq = if (lengthOf folded seq == 1) then Una uop (headOf folded seq & fromJust) else Bin bop (seq & mapped %~ (Una uop))
unfoldBin bop seq = if (lengthOf folded seq == 1) then (headOf folded seq & fromJust) else Bin bop seq
}
