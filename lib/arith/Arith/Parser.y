{
module Arith.Parser where

import qualified Arith.Lexer as L
import Arith.Alex (Loc(..), Token, AlexInput)
import Arith.Error
import qualified Arith.Alex as Tok
import Arith.Expr
import Arith.Parse.Monad    

import Optics
import Optics.State.Operators

import qualified Control.Monad.Except as E

}

%name arith expr
%tokentype { (Loc Token) }
%error { (E.throwError . ParseErr) }
%lexer { (L.lex >>=) } { (Loc _ Tok.EOF) }
%monad { ParseM Err }

%token
'+'    { Loc _ Tok.Add }
'-'    { Loc _ Tok.Neg }
'*'    { Loc _ Tok.Mul }
'/'    { Loc _ Tok.Slash }
'('    { Loc _ Tok.OpenParen }
')'    { Loc _ Tok.CloseParen }
int    { Loc _ (Tok.Int $$) }
float  { Loc _ (Tok.Float $$) }
double { Loc _ (Tok.Double $$) }

%left '+' 
%left '*'
%left '-'
%right '/'

%%

expr :: { Expr }
     : expr '+' term { Add $1 $3 }
     | expr '-' term { Add $1 (Neg $3) }
     | term { $1 }

term :: { Expr }
     : term '*' factor { Mul $1 $3 }
     | term '/' factor { Mul $1 (Inv $3) }
     | factor { $1 }

factor :: { Expr }
       : int { Prim (Int $1) }
       | float { Prim (Float $1) }
       | double { Prim (Double $1) }
       | '-' factor { Neg $2  }
       | '(' expr ')' { $2  }
{
  
}
