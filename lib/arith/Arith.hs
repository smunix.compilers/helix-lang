-- |
module Arith where

import Arith.Expr (Expr)
import Arith.Parse.Monad (parse)
import Arith.Parser (arith)
import GHC.Exts (IsString (..))

instance IsString Expr where
  fromString = either (error . show) id . parse arith

-- $> "0f + 1 / 1.0f / 2.0 + 3" :: Expr
