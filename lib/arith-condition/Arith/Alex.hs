module Arith.Alex where

import Data.Word (Word8)
import Optics

data Token where
  Int :: Int -> Token
  Float :: Float -> Token
  Double :: Double -> Token
  Bool :: Bool -> Token
  Add :: Token
  Neg :: Token
  Mul :: Token
  Slash :: Token
  OpenParen :: Token
  CloseParen :: Token
  OpenBracket :: Token
  CloseBracket :: Token
  Comma :: Token
  EOF :: Token
  If :: Token
  Then :: Token
  Else :: Token
  deriving (Show)

data AlexSourcePos where
  AlexSourcePos ::
    { _line :: !Int,
      _col :: !Int
    } ->
    AlexSourcePos
  deriving (Show)

makeLenses ''AlexSourcePos

data AlexInput where
  AlexInput ::
    { _pos :: AlexSourcePos,
      _char :: Char,
      _pending :: [Word8],
      _input :: String
    } ->
    AlexInput
  deriving (Show)

makeLenses ''AlexInput

data Loc a where
  Loc ::
    { _loc :: AlexSourcePos,
      _token :: a
    } ->
    Loc a
  deriving (Show)

makeLenses ''Loc
