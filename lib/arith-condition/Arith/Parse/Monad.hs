{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Arith.Parse.Monad where

import Arith.Alex (AlexInput (AlexInput), AlexSourcePos (AlexSourcePos))
import Control.Monad.Error.Class (MonadError)
import Control.Monad.State.Class (MonadState)
import Control.Monad.Trans.State (StateT, evalStateT)
import Optics

newtype ParseM e a where
  ParseM :: {_run :: StateT AlexInput (Either e) a} -> ParseM e a
  deriving (Functor, Applicative, Monad, MonadState AlexInput, MonadError e)

makeLenses ''ParseM

parse :: ParseM e a -> String -> Either e a
parse p str = evalStateT (p ^. run) (AlexInput (AlexSourcePos 1 1) '\n' Empty str)
