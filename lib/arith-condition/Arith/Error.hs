{-# LANGUAGE StandaloneDeriving #-}

-- |
module Arith.Error where

import Arith.Alex
import Optics

class Render e where
  render :: e -> Error

newtype Error = Error {_error :: String}
  deriving (Show)

makeLenses ''Error

data Err where
  LexErr :: (Show e) => {_errLexer :: e} -> Err
  ParseErr :: (Show e) => {_errParser :: e} -> Err

makeLenses ''Err

deriving instance Show Err

instance Render Err where
  render = Error . show
